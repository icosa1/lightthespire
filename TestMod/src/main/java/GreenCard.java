import basemod.abstracts.AbstractCardModifier;
import basemod.helpers.CardBorderGlowManager;
import com.badlogic.gdx.graphics.Color;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.rooms.AbstractRoom;

public class GreenCard extends AbstractCardModifier {
    StringBuilder s = new StringBuilder();
    String modid = "GreenCard";
    public GreenCard() {
        CardBorderGlowManager.addGlowInfo(new CardBorderGlowManager.GlowInfo() {
            @Override
            public boolean test(AbstractCard card) {
                //return true if "card" follows this rule, else return false
                return true;
            }

            @Override
            public Color getColor(AbstractCard card) {
                    return Color.GREEN.cpy();
                //return an instance of Color to be used as the color. e.g. Color.WHITE.cpy().
            }

            @Override
            public String glowID() {
                return modid;
                //return a string to be used as a unique ID for this glow.
                //It's recommended to follow the usual modding convention of "modname:name"
            }
        });
    }

    @Override
    public String identifier(AbstractCard card) {
        return modid;
    }

    @Override
    public String modifyDescription(String rawDescription, AbstractCard card) {


        if(AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT){
            for(AbstractMonster m : AbstractDungeon.getCurrRoom().monsters.monsters){
                //s.append(m.id);
            }
        }
        return  rawDescription + s;
    }

    @Override
    public AbstractCardModifier makeCopy() {
        return new GreenCard();
    }

}
