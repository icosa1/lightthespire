import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.evacipated.cardcrawl.modthespire.lib.SpireInitializer;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.evacipated.cardcrawl.modthespire.lib.SpireEnum;

import basemod.BaseMod;
import basemod.interfaces.PostDrawSubscriber;
import basemod.interfaces.PostUpdateSubscriber;
import basemod.interfaces.PreRoomRenderSubscriber;
import basemod.helpers.CardModifierManager;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.rooms.AbstractRoom;
import java.util.HashMap;

@SpireInitializer
public class LighttheSpire implements PostDrawSubscriber {

    public LighttheSpire() {
        BaseMod.subscribe(this);
    }

    public static void initialize() {
        new LighttheSpire();
    }

    @Override
    public void receivePostDraw(AbstractCard card){
//        CardGroup Deck = AbstractDungeon.player.masterDeck;
//        CardGroup deck = AbstractDungeon.player.masterDeck;
//
//        ArrayList<String> alpha = new ArrayList<>();
//
//        for(AbstractCard c : deck.group){
//            alpha.add(c.cardID);
//        }
//        ArrayList<String> beta;
//
//        CardGroup h = AbstractDungeon.player.hand;
//
//        for(AbstractCard c : h.group){
//            if(CardModifierManager.hasModifier(c, "textedcard")){
//                CardModifierManager.removeAllModifiers(c, false);
//            }
            //make deck without hand's card
//            beta = (ArrayList<String>)alpha.clone();
//            beta.remove(card.cardID);
//        }
        HashMap<String, Float> deck = new HashMap<String, Float>(){{
            put("Defend_R", new Float(0.6609504));
            put("Strike_R", new Float(0.5299158));
            put("Seeing Red", new Float(-0.1764154));
            put("Bash", new Float(-0.4023786));
            put("Sever Soul", new Float(-1.5718209));
        }};

        //if(!CardModifierManager.hasModifier(card, "textedcard"))
        if(Float.compare(deck.get(card.cardID), 0.4f) > 0f)
            card.glowColor = Color.GREEN.cpy();
        else if(Float.compare(deck.get(card.cardID), -0.2f) > 0f)
            card.glowColor = Color.ORANGE.cpy();
        else
            card.glowColor = Color.RED.cpy();
            //CardModifierManager.removeEndOfTurnModifiers(card);
            //CardModifierManager.removeWhenPlayedModifiers(card);

//    @Override
//    public void receivePreRoomRender(SpriteBatch sb){
//        AbstractDungeon d = CardCrawlGame.dungeon;
//        //If in combat
//        if(d != null && AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT){
//            CardGroup h = AbstractDungeon.player.hand;
//            for(AbstractCard c : h.group){
//                if(!CardModifierManager.hasModifier(c, "textedcard"))
//                    CardModifierManager.addModifier(c, new Textedcard());
//                }
//        }
    }
}